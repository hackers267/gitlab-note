# 常见问题

## 如果自动关闭相关的Issue?

在`gitlab`中，如果要自动关闭`Issue`，可以在提交的信息中使用下面的关键字+issue的id。

关键字：
* Close,Closes,Closed,Closing,close,closes,closed,closing
* Fix,Fixes,Fixed,Fixing,fix,fixes,fixed,fixing
* Resolve,Resolves,Resolved,Resolving,resolve,resolves,resolves,resolving
* Implement,Implements,Implemented,Implementing,implement,implements,implemented,implementing

比如下面的提交信息：
```text
Awesome commit message

Fix #20, Fixes #21 and Closes group/otherproject#22.
This commit is also related to #17 and fixes #18, #19
and https://gitlab.example.com/group/otherproject/issues/23.
```

这个提交信息会自动关闭`#20`,`#21`,`#18`,`#19`和`otherproject`中的`#22`,`#23`的issue,但不会关闭`#17`的issue,因为`#17`不符合上面的规则。
