# 命令行

```shell
# 运行所有GitLab组件
sudo gitlab-ctl start
# 停止所有GitLab组件
sudo gitlab-ctl stop
# 重启GitLab组件
sudo gitlab-ctl restart
```

> 注意，如果你使用的是`docker`技术安装的`GitLab`，那么在执行命令的时候，需要去掉`sudo`并使用`docker exec -it [CONTAINER]`代替
