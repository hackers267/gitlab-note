# Summary

- [自托管](./self_host/ReadMe.md)
- [CI/CD](./ci_cd/ReadMe.md)
  - [部署前端应用](./ci_cd/astro.md)
- [Pages](./pages.md)
- [QA](./qa/ReadMe.md)
