# 使用GitLab自动化部署Astro到自己的服务器

如果我们正在开发一个`Astro`应用，并且是在`GitLab`上托管，那么我们就可以使用`GitLab`的自动化部署来把我们的应用部署到自己的服务器上。

## 设置自动化部署需要的变量

在我们的最终的代码里，我们需要使用到多个变量，所以在书写代码之前，我们需要先设置这些变量。

首先通过`设置-CI/CD`打开`CI/CD`设置，点击`变量`一栏中的展示，查看已经设置的变量。如果你是第一次使用，那么在列表中应该是没有变量的。

然后在表格右上角中点击*添加变量*，在弹出的窗口中，分别设置*键*和*值*，设置完成后，点击*添加变量*,然后继续添加其他变量。在部分Astro应用到自己的服务器时，需要添加以下4个变量：

- HOST - 部署服务器的域名
- USER - 部署服务器的用户名
- USER_PASSWORD - 部署服务器的用户名对应的密码
- UPLOAD_DIR - 部署服务器的上传目录

## 书写`.gitlab-ci.yml`文件

要想使用GitLab自动化部署Astro到自己的服务器，我们需要书写一个`.gitlab-ci.yml`文件。

为了方便理解，我们来一步一步书写这个`.gitlab-ci`文件，最后，我们把所有的内容都合并到一个`.gitlab-ci.yml`文件中。

我们把整个过程分为三个阶段：_安装_, *构建*和*部署*

### 安装阶段

因为我们要部署的是一个`Astro`应用，其依赖`NodeJS`，所以我们需要使用`NodeJS`的镜像。而`Astro`对`NodeJS`的最低版本要求是`18.14.1`，所以我们直接使用`NodeJS`的`18.14.1`版本。

所以就有了以下代码:

```yml
image: node:18.14.1
```

在安装的过程中，我们需要使用`pnpm`来安装`Astro`的依赖，而为了构建部署的速度，我们需要使用`GitLab`的缓存功能，因为`pnpm`的需要，我们除了缓存`NodeJS`的`node_modules`之外，我们还需要缓存`.pnpm-store`和依据`pnpm-lock.yaml`来判断缓存是否失效。所以在上面的代码的基础之后，添加下面的代码：

```yml
cache:
  paths:
    - node_modules
    - .pnpm-store
  key:
    files:
      - pnpm-lock.yaml
```

为了使用`pnpm`安装代码，我们需要在安装阶段的`before_script`中添加以下代码:

```yml
before_script:
  - corepack enable
  - corepack prepare pnpm@latest-8 --activate
  - pnpm config set store-dir .pnpm-store
```

好了，为安装阶段所做的准备就完成了，下面我们把安装阶段的代码总结一下：

```
image: node:18.14.1

cache:
  paths:
    - node_modules
    - .pnpm-store
  key:
    files:
      - pnpm-lock.yaml

stages:
  - install

install:
  stage: install
  before_script:
    - corepack enable
    - corepack prepare pnpm@latest-8 --activate
    - pnpm config set store-dir .pnpm-store
  script:
    - pnpm install # 正式安装依赖

```

安装阶段完成后，我们接下来就要开始构建阶段了。

## 构建阶段

在构建阶段中，我们要构建了我们需要部署到服务上的最终产物，为了在部署阶段使用在构建阶段产生的产物，我们需要配置`artifacts`选项如下:

```yml
artifacts:
  untracked: false
  when: on_success
  expire_in: 30 days
  paths:
    - dist/
```

在这个配置中，我们需要在这个阶段运行成功后才使用产物，配置了`when: on_success`选项，构建产物的有效期通过`expire_in`选项设置为30天。产物的选取路径通过`paths`选项配置为`dist/`。

构建产物的前提我们需要成功运行`install`阶段，所以需要添加`needs`选项：

```yml
needs:
  - install
```

这些都设置完成后，我们只要在构建中使用我们的构建命令`npm run build`就可以了。

构建阶段的代码如下：

```yml
stages:
  - install
  - build

# 安装阶段的安装参考上面

build:
  stage: build
  script:
    - npm run build # 构建产物
  needs:
    - install
  artifacts:
    untracked: false
    when: on_success
    expire_in: 30 days
    paths:
      - dist/
```

> 在构建构建中不要使用`pnpm`来运行`build`命令，因为在这个阶段中，并没有`pnpm`命令，其和安装阶段中的`pnpm`没有关系。

下面就是最终的部署阶段了。

## 部署阶段

在部署阶段，我们需要使用到两个工具`scp`和`sshpass`。其中`scp`用来把我们的最终产物上传到服务器，而`sshpass`用来为`scp`传递密码。

因为我们使用的`NodeJS`镜像中没有`sshpass`工具，所以我们需要先安装它：

```yml
before_script:
  - apt-get update
  - apt-get install sshpass -y
```

> 注意，在第二个命令`apt-get install sshpass -y`中，`-y`是必不可少的。如果没有`-y`选项，`apt-get`命令会等待你确认输入，最后会因为等待超时报错。

部署阶段的准备工作差不多了，我们总结一下代码：

```yml
stages:
  - install
  - build
  - deploy

deploy:
  stage: deploy
  before_script:
    - apt-get update
    - apt-get install sshpass -y
  script:
    - sshpass -p $USER_PASSWORD scp -o StrictHostKeyChecking=no -P22 -r dist/ $USER@$HOST:$UPLOAD_DIR
```

在最终的部署阶段中，我们使用到了我们在最开始设置的`CI/CD`的变量。

## 最终的`.gitlab-ci.yml`

`.gitlab-ci.yml`文件代码：

```yml
image: node:18.14.1

cache:
  paths:
    - node_modules
    - .pnpm-store
  key:
    files:
      - pnpm-lock.yaml

stages:
  - install
  - build
  - deploy

install:
  stage: install
  before_script:
    - corepack enable
    - corepack prepare pnpm@latest-8 --activate
    - pnpm config set store-dir .pnpm-store
  script:
    - pnpm install # install dependencies

build:
  stage: build
  script:
    - npm run build # build project
  needs:
    - install
  artifacts:
    untracked: false
    when: on_success
    expire_in: 30 days
    paths:
      - dist/

deploy:
  stage: deploy
  before_script:
    - apt-get update
    - apt-get install sshpass -y
  script:
    - sshpass -p $USER_PASSWORD scp -o StrictHostKeyChecking=no -P22 -r dist/ $USER@$HOST:$UPLOAD_DIR
```

## 总结

通过这个文章，我们实现了通过`GitLab`自动部署我们的`Astro`应用到我们的服务器中。其中，我们还可以在部署阶段做一些其他事情，比如备份之前的应用版本，执行我们在服务器中的自定义脚本等。在这里我们仅仅实现了最简单的部署操作。其实，我们还可以通过`GitLab`的自动化集成/自动化部署功能来运行我们自己的自动化测试。这个文章中的内容，不仅仅适用于`Astro`在`GitLab`中的自动化部署，还适用于`React`或`Vue`等应用的自动化部署。
